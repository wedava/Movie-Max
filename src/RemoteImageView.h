/*
 * RemoteImageView.h
 *
 *  Created on: Dec 14, 2015
 *      Author: alphabuddha
 */

#ifndef REMOTEIMAGEVIEW_H_
#define REMOTEIMAGEVIEW_H_

#include <bb/cascades/CustomControl>
#include <bb/cascades/Container>
#include <bb/cascades/ImageView>
#include <bb/cascades/Container>
#include <QTNetwork>
#include <bb/cascades/controls/activityindicator.h>

namespace bb {
    namespace cascades {
        class Container;
    }
}
using namespace bb::cascades;

class RemoteImageView: public CustomControl {
    Q_OBJECT
    Q_PROPERTY(QString url READ URL WRITE seturl NOTIFY urlChanged)

public:
    RemoteImageView(Container *parent=0);
    virtual ~RemoteImageView();
    Container* stuff;
    ImageView* imageView;
    Q_INVOKABLE void loadImage();
    Q_INVOKABLE bb::ImageData fromQImage(const QImage &qImage);
    void seturl(QString url);
    QString URL();
    ActivityIndicator* indicator;

public  slots:
    void onImageLoaded(QNetworkReply* reply);
    void onurlChanged();
    signals:
            void imageUnavailable();
            void urlChanged(QString url);

private:
    QString murl;

};

#endif /* REMOTEIMAGEVIEW_H_ */
