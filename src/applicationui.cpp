/* Copyright (c) 2012, 2013, 2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "moviemaxapp.h"
#include "RemoteImageView.h"
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/TabbedPane>
#include <bb/cascades/Tab>
#include <bb/cascades/Page>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/DataModel>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/datamanager/AsyncDataModel>
#include <bb/cascades/ListView>
#include <bb/cascades/GridLayout> //for GridListLayout
#include <bb/cascades/ActivityIndicator>

using namespace bb::cascades;
using namespace bb::data;


MovieMaxApp::MovieMaxApp(QObject *parent) : QObject(parent)
{
    // Register QML types, so they can be used in QML.
    //qmlRegisterType<SqlHeaderDataQueryEx>("bb.cascades.datamanager", 1, 2, "SqlHeaderDataQueryEx");
    //qmlRegisterType<PullToRefresh>("com.weather", 1, 0, "PullToRefresh");
    //qmlRegisterType<CityDataSource>("com.weather", 1, 0, "CityDataSource");
    //qmlRegisterType<LoadModelDecorator>("com.weather", 1, 0, "LoadModelDecorator");
    //qmlRegisterType<WeatherDataSource>("com.weather", 1, 0, "WeatherDataSource");
    //qmlRegisterUncreatableType<WeatherError>("com.weather", 1, 0, "WeatherError", "Uncreatable type");
    qmlRegisterType<RemoteImageView>("com.BinaryBanner", 1, 0, "ImageDownloader");

    // Prepare localization. Connect to the LocaleHandlers systemLanguaged change signal, this will
    // tell the application when it is time to load a new set of language strings.
    mTranslator = new QTranslator(this);
    mLocaleHandler = new LocaleHandler(this);
    onSystemLanguageChanged();
    bool connectResult = connect(mLocaleHandler, SIGNAL(systemLanguageChanged()), SLOT(onSystemLanguageChanged()));
    Q_ASSERT(connectResult);
    Q_UNUSED(connectResult);

    // Create a QMLDocument and load it, using build patterns.
    QmlDocument *qmlDocument = QmlDocument::create("asset:///main.qml").parent(this);
    qmlDocument->setContextProperty("app", this);

    if (!qmlDocument->hasErrors()) {


        TabbedPane *appPane = qmlDocument->createRootObject<TabbedPane>();
        if (appPane) {
                    // Set the main application scene to NavigationPane.
                    Application::instance()->setScene(appPane);
                }

        QObject::connect(appPane, SIGNAL(activeTabChanged(bb::cascades::Tab*)), this, SLOT(sectionTriggered(bb::cascades::Tab*)));
    }
}

MovieMaxApp::~MovieMaxApp()
{
}

void MovieMaxApp::onSystemLanguageChanged()
{
    // Remove the old translator to make room for the new translation.
    QCoreApplication::instance()->removeTranslator(mTranslator);

    // Initiate, load and install the application translation files.
    QString localeString = QLocale().name();
    QString fileName = QString("movie-max_%1").arg(localeString);
    if (mTranslator->load(fileName, "app/native/qm")) {
        QCoreApplication::instance()->installTranslator(mTranslator);
    }
}

void MovieMaxApp::sectionTriggered(bb::cascades::Tab* activeTab) {
    QString tabName = activeTab->objectName();
    if (tabName=="showingTab"){
        //Container *stuff = new Container();
        //indicator->setHorizontalAlignment(HorizontalAlignment::Center);
        //indicator->setVerticalAlignment(VerticalAlignment::Center);
        //indicator->setPreferredSize(400.0, 400.0);
        AbstractPane *current = Application::instance()->scene();
        ActivityIndicator *myIndicator = current->findChild<ActivityIndicator*>("myIndicator");
        myIndicator->setVisible(true);
        myIndicator->start();
        MovieMaxApp::discover();
    }
    else if (tabName=="upcomingTab"){
            //Container *stuff = new Container();
            //indicator->setHorizontalAlignment(HorizontalAlignment::Center);
            //indicator->setVerticalAlignment(VerticalAlignment::Center);
            //indicator->setPreferredSize(400.0, 400.0);
            AbstractPane *current = Application::instance()->scene();
            ActivityIndicator *myIndicator = current->findChild<ActivityIndicator*>("myIndicator");
            myIndicator->setVisible(true);
            myIndicator->start();
            MovieMaxApp::upcoming();
        }
    else if (tabName=="popularTab"){
                //Container *stuff = new Container();
                //indicator->setHorizontalAlignment(HorizontalAlignment::Center);
                //indicator->setVerticalAlignment(VerticalAlignment::Center);
                //indicator->setPreferredSize(400.0, 400.0);
                AbstractPane *current = Application::instance()->scene();
                ActivityIndicator *myIndicator = current->findChild<ActivityIndicator*>("myIndicator");
                myIndicator->setVisible(true);
                myIndicator->start();
                MovieMaxApp::popular();
            }
    qDebug() << "Tab changed to " << tabName;

}

void MovieMaxApp::discover()
    {
    //AbstractPane *current = Application::instance()->scene();
    //ActivityIndicator *myIndicator = current->findChild<ActivityIndicator*>("myIndicator");
    //myIndicator->start();
    //myIndicator->setVisible(true);
    QNetworkAccessManager* networkAccessManager = new QNetworkAccessManager(this);
    const QString queryUri = "https://joseph-n-movie-max-v1.p.mashape.com/v1/movies";
    QNetworkRequest request(queryUri);
    request.setRawHeader("X-Mashape-Key", "RRHaWQNrd8mshIhV1fHj71kPRvdrp1rOkeSjsnEbpbJ8E3D5XO");
    QNetworkReply* reply = networkAccessManager->get(request);
    bool ok = connect(reply, SIGNAL(finished()), this, SLOT(onFinished()));
    Q_ASSERT(ok);
    Q_UNUSED(ok);
}

void MovieMaxApp::upcoming()
    {
    QNetworkAccessManager* networkAccessManager = new QNetworkAccessManager(this);
    const QString queryUri = "https://joseph-n-movie-max-v1.p.mashape.com/v1/movies/upcoming";
    QNetworkRequest request(queryUri);
    request.setRawHeader("X-Mashape-Key", "RRHaWQNrd8mshIhV1fHj71kPRvdrp1rOkeSjsnEbpbJ8E3D5XO");
    QNetworkReply* reply = networkAccessManager->get(request);
    bool ok = connect(reply, SIGNAL(finished()), this, SLOT(onFinished()));
    Q_ASSERT(ok);
    Q_UNUSED(ok);
}

void MovieMaxApp::cinemas()
    {
    QNetworkAccessManager* networkAccessManager = new QNetworkAccessManager(this);
    const QString queryUri = "https://joseph-n-movie-max-v1.p.mashape.com/v1/theatres";
    QNetworkRequest request(queryUri);
    request.setRawHeader("X-Mashape-Key", "RRHaWQNrd8mshIhV1fHj71kPRvdrp1rOkeSjsnEbpbJ8E3D5XO");
    QNetworkReply* reply = networkAccessManager->get(request);
    bool ok = connect(reply, SIGNAL(finished()), this, SLOT(onFinished()));
    Q_ASSERT(ok);
    Q_UNUSED(ok);
}

void MovieMaxApp::onFinished()
    {
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    qDebug() << "Response is " << reply;
    qDebug() << "Response code is " << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    qDebug() << "Response code is " << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 200)
    {
       // JsonDataAccess jda;
        //QVariantMap map = jda.loadFromBuffer(reply->readAll()).toMap();
        //QVariantList addresses = map["GetCategoryResult"].toList();
        //foreach(QVariant var, addresses) {
        //    QVariantMap addressMap = var.toMap();
        //    qDebug() << "CategoryName is " << addressMap["CategoryName"].toString();
        //    qDebug() << "CategoryID is " << addressMap["CategoryID"].toString();
        //}

        // Load the JSON data
        GroupDataModel *popularModel = new GroupDataModel();
        JsonDataAccess jda;
        QVariantList list = jda.loadFromBuffer(reply->readAll()).toList();
        foreach(QVariant var, list) {
                    QVariantMap addressMap = var.toMap();
                    qDebug() << sizeof(popularModel);
                   qDebug() << "Image title: " << addressMap["poster_url"].toString();
                   addressMap.insert("image", "asset:///images/cinemas.png");
                   popularModel->insert(addressMap);

        }

        // Add the data to the model
        //popularModel->insertList(list);
        AbstractPane *current = Application::instance()->scene();
        ActivityIndicator *myIndicator = current->findChild<ActivityIndicator*>("myIndicator");
        myIndicator->stop();
        myIndicator->setVisible(false);

        ListView* showing = current->findChild<ListView*>("listview");
        //showing->setLayout(new GridListLayout);

        // Add your model to a ListView
        showing->setDataModel(popularModel);
        showing->requestFocus();
        qDebug() << "Request worked.";
        qDebug() << sizeof(popularModel);
    }
    else {
    qDebug() << "Server returned code " << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    }
}

void MovieMaxApp::popular()
    {
    QNetworkAccessManager* networkAccessManager = new QNetworkAccessManager(this);
    QString api_key = "53d64bfc413abe1c8a34f21f8d59a939";
    QString queryUri = "http://api.themoviedb.org/3/movie/popular?api_key=" + api_key;
    qDebug() << "URL IS:  " << queryUri;
    QNetworkRequest request(queryUri);
    //request.setRawHeader("Accept", "application/json");
    QNetworkReply* reply = networkAccessManager->get(request);
    qDebug() << "popular activated";
    bool ok = connect(reply, SIGNAL(finished()), this, SLOT(onFinished1()));
    Q_ASSERT(ok);
    Q_UNUSED(ok);
}

void MovieMaxApp::onFinished1()
    {
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    qDebug() << "Response is " << reply;
    qDebug() << "Response code is " << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    qDebug() << "Response code is " << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 200)
    {
       // JsonDataAccess jda;
        //QVariantMap map = jda.loadFromBuffer(reply->readAll()).toMap();
        //QVariantList addresses = map["GetCategoryResult"].toList();
        //foreach(QVariant var, addresses) {
        //    QVariantMap addressMap = var.toMap();
        //    qDebug() << "CategoryName is " << addressMap["CategoryName"].toString();
        //    qDebug() << "CategoryID is " << addressMap["CategoryID"].toString();
        //}

        // Load the JSON data
        GroupDataModel *popularModel = new GroupDataModel();
        JsonDataAccess jda;
        QVariantList list;
        QByteArray a = reply->readAll();
        a.insert(0, '[');
        a.append(']');
        list = jda.loadFromBuffer(a).value<QVariantList>();
        qDebug() << "LIST IS:  " << list;
        foreach(QVariant var, list) {
                   qDebug() << "VAR IS: " << var;
                   QVariantMap Map = var.toMap();
                   qDebug() << "MAP IS: " << Map;
                   QVariantList results = Map["results"].toList();
                   qDebug() << "RESULTS ARE: " << results;
                   foreach(QVariant var1, results){
                       QVariantMap item = var1.toMap();
                       qDebug() << "Title is: " << item["original_title"];
                       QString image = "http://image.tmdb.org/t/p/w154" + item["poster_path"].toString();
                       item.insert("web_image", image);
                       item.insert("image", "asset:///images/cinemas.png");
                       popularModel->insert(item);
                   }

        }

        // Add the data to the model
        //popularModel->insertList(list);
        AbstractPane *current = Application::instance()->scene();
        ActivityIndicator *myIndicator = current->findChild<ActivityIndicator*>("myIndicator");
        myIndicator->stop();
        myIndicator->setVisible(false);

        ListView* showing = current->findChild<ListView*>("listview");
        //showing->setLayout(new GridListLayout);

        // Add your model to a ListView
        showing->setDataModel(popularModel);
        showing->requestFocus();
        qDebug() << "Request worked.";
        qDebug() << sizeof(popularModel);
    }
    else {
    qDebug() << "Server returned code " << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    }
}


