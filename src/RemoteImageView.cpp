#include "RemoteImageView.h"
#include <bb/cascades/Container>
#include <bb/cascades/ImageView>
#include <bb/cascades/ScalingMethod>
#include <bb/cascades/DockLayout>
#include <QtCore/QByteArray>
#include <bb/ImageData>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/Application>

using namespace bb::cascades;

bb::ImageData RemoteImageView::fromQImage(const QImage &qImage) {
     bb::ImageData imageData(bb::PixelFormat::RGBA_Premultiplied, qImage.width(),
     qImage.height());

    unsigned char *dstLine = imageData.pixels();
     for (int y = 0; y < imageData.height(); y++) {
         unsigned char * dst = dstLine;
         for (int x = 0; x < imageData.width(); x++) {
             QRgb srcPixel = qImage.pixel(x, y);
             *dst++ = qRed(srcPixel);
             *dst++ = qGreen(srcPixel);
             *dst++ = qBlue(srcPixel);
             *dst++ = qAlpha(srcPixel);
         }
         dstLine += imageData.bytesPerLine();
     }

    return imageData;
}


RemoteImageView::RemoteImageView(Container *parent) :
        CustomControl(parent) {
	//Q_UNUSED(parent);
    //AbstractPane *current = Application::instance()->scene();
	Container *stuff = new Container();
	stuff->setLayout(new DockLayout);
	imageView = ImageView::create().image(
			QUrl("asset:///images/defaultarticlelist.png")).horizontal(
			HorizontalAlignment::Center).vertical(VerticalAlignment::Center);
	//imageView = ImageView::create().image(
	//          QUrl("asset:///images/cinemas.png"));
	//imageView->setPreferredSize(500, 900);
	//imageView->setScalingMethod(bb::cascades::ScalingMethod::AspectFill);
	stuff->add(imageView);
	indicator = new ActivityIndicator();
	indicator->setPreferredSize(60.0, 60.0);
	indicator->setHorizontalAlignment(HorizontalAlignment::Center);
	indicator->setVerticalAlignment(VerticalAlignment::Center);
	stuff->add(indicator);

	setRoot(stuff);
	connect(this, SIGNAL(urlChanged(QString)), this, SLOT(onurlChanged()));
	//  connect(mRootContainer->layout(), SIGNAL(CreationCompleted()), this, SLOT(onurlChanged()));

}

RemoteImageView::~RemoteImageView() {
	//delete mRootContainer;
}

void RemoteImageView::loadImage() {
    indicator->start();
	qDebug() << murl;
	QNetworkRequest request = QNetworkRequest();
	murl.replace(QString("w92"), QString("w154"));
	qDebug() << "Image URL is: " << murl;
	request.setUrl(QUrl(murl));
	QNetworkAccessManager* nam = new QNetworkAccessManager(this);
	bool result = connect(nam, SIGNAL(finished(QNetworkReply*)), this,
			SLOT(onImageLoaded(QNetworkReply*)));
	Q_ASSERT(result);
	Q_UNUSED(result);

	nam->get(request);
}
void RemoteImageView::onurlChanged() {
	loadImage();

}
void RemoteImageView::onImageLoaded(QNetworkReply* reply) {
	if (reply->error() != QNetworkReply::NoError) {
		emit imageUnavailable();
		indicator->stop();
		return;
	}
	QByteArray image = reply->readAll();

	QImage qimage;
	qimage.loadFromData(image);

	int width = qimage.width();
	int height = qimage.height();
	QImage scaled = qimage.scaled(720, 720, Qt::KeepAspectRatio);
	//PixelBufferData pixelBuffer = PixelBufferData(PixelBufferData::RGBX, width, height, width, qimage.bits());
	imageView->setImage(fromQImage(scaled));
	indicator->stop();

	//Image pic = Image(pixelBuffer);
	//imageView->setImage(pic);
	//Image image = Image(reply->readAll());
	//imageView->setImage(image);

}
void RemoteImageView::seturl(QString url) {
	if (murl.compare(url) != 0) {
		murl = url;
		emit urlChanged(murl);
	}
}
QString RemoteImageView::URL() {
	return murl;
}



