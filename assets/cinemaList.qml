import bb.cascades 1.4
import bb.cascades.datamanager 1.2
import com.BinaryBanner 1.0  // For registered QML types

NavigationPane {
    id: navigationPane
    
    Page {
        titleBar: TitleBar {
            title : "CINEMAS"
            scrollBehavior: TitleBarScrollBehavior.NonSticky
            //visibility: ChromeVisibility.Compact
        }
        onCreationCompleted: {
            app.cinemas()
            myIndicator.start()
        }
        Container {
            layout: DockLayout {
            
            }
            margin.bottomOffset: 120
            
            Container {
                ActivityIndicator {
                    id: myIndicator
                    objectName: "myIndicator"
                    preferredWidth: 800
                    preferredHeight: 800
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    visible: false
                }
                
                id: stuff
                ListView {
                    id: listview
                    objectName: "listview"
                    dataModel: dataModel
                    scrollRole: ScrollRole.Main
                    layout: StackListLayout {
                        headerMode: ListHeaderMode.Standard
                    }
                    
                    //horizontalAlignment: HorizontalAlignment.Fill
                    //verticalAlignment: VerticalAlignment.Fill
                    //layout: GridListLayout {
                    //    orientation: LeftToRight
                    
                    //}
                    
                    //visible: false
                    listItemComponents: [
                        ListItemComponent {
                            type: "item"
                            StandardListItem {
                                imageSpaceReserved: true
                                imageSource: "asset:///images/cinlist.png"
                                //imageSource: ListItemData.image
                                title: ListItemData.name
                                description: ListItemData.location
                            }
                        
                        
                        }
                    
                    ]
                    attachedObjects: [
                        GroupDataModel {
                            id: dataModel
                            grouping: ItemGrouping.None
                        }
                    ]
                } // ListView
            }
        }     
        
        
        actions: ActionItem {
            title: qsTr("Refresh")
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/reload.png"
            
            //onTriggered: {
            //    navigationPane.push(secondPageDefinition.createObject());
            //}
        }
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: secondPageDefinition
            Page {
                Container {
                
                }
            }
        }
    ]
    
    onPopTransitionEnded: {
        page.deleteLater();
    }
}
