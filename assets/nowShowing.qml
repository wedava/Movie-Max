import bb.cascades 1.4
import bb.cascades.datamanager 1.2
import com.BinaryBanner 1.0  // For registered QML types

NavigationPane {
    id: navigationPane
    
    Page {
        titleBar: TitleBar {
            title : "NOW SHOWING"
            scrollBehavior: TitleBarScrollBehavior.NonSticky
            //visibility: ChromeVisibility.Compact
        }
        onCreationCompleted: {
            app.discover()
            myIndicator.start()
        }
        Container {
            layout: DockLayout {
                
            }
            margin.bottomOffset: 120
        
        Container {
            ActivityIndicator {
                id: myIndicator
                objectName: "myIndicator"
                preferredWidth: 70
                preferredHeight: 1000
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                visible: false
            }

            id: stuff
            ListView {
                id: listview
                objectName: "listview"
                dataModel: dataModel
                scrollRole: ScrollRole.Main
                layout: GridListLayout {
                    columnCount: 4
                    headerMode: ListHeaderMode.Standard
                    cellAspectRatio: 0.7
                    //spacingAfterHeader: 20
                    verticalCellSpacing: 7
                }
                
                //horizontalAlignment: HorizontalAlignment.Fill
                //verticalAlignment: VerticalAlignment.Fill
                //layout: GridListLayout {
                //    orientation: LeftToRight
                
                //}
                
                //visible: false
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        //type: ""
                        CustomListItem {
                            
                            dividerVisible: true
                            highlightAppearance: HighlightAppearance.Frame
                            ImageDownloader {
                                id: webViewImage
                                url: ListItemData.poster_url
                                //horizontalAlignment: HorizontalAlignment.Center
                                //verticalAlignment: VerticalAlignment.Center
                            }
                        }
                        
                                      
                    }
                    
                ]
                attachedObjects: [
                    AsyncHeaderDataModel {
                        id: dataModel
                        //grouping: ItemGrouping.None
                        cacheSize: 1000
                    }
                ]
            } // ListView
        }
   }     
        
        
        actions: ActionItem {
            title: qsTr("Refresh")
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/reload.png"
            
            onTriggered: {
                navigationPane.push(secondPageDefinition.createObject());
            }
        }
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: secondPageDefinition
            Page {
                Container {
                    
                }
            }
        }
    ]
    
    onPopTransitionEnded: {
        page.deleteLater();
    }
}
