/*
 * Copyright (c) 2011-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.4

TabbedPane {
    id: mainTabbedPane
    objectName: "mainTabbedPane"
    Menu.definition: MenuDefinition {
        settingsAction: SettingsActionItem {
            onTriggered: {
                               
            }
        }
        helpAction: HelpActionItem {
            onTriggered: {
                
            }
        }
        
        actions: [
            ActionItem {
                title: "Share"
                imageSource: "asset:///images/share.png"
                onTriggered: {
                }
            },
            ActionItem {
                title: "Rate"
                imageSource: "asset:///images/rate.png"
                onTriggered: {
                }
            }
        ]
    }
    
    showTabsOnActionBar: true
    peekEnabled: true
    sidebarState: SidebarState.VisibleFull
    Tab { //First tab
        // Localized text with the dynamic translation and locale updates support
        id: showingTab
        objectName: "showingTab"
        title: qsTr("Now Showing") + Retranslate.onLocaleOrLanguageChanged
        imageSource: "asset:///images/showing.png"
        delegate: Delegate {
            id: showingTabDelegate
            source: "nowShowing.qml" // tab1 contents is loaded from popularMovies.qml
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.Default
    } //End of first tab
    Tab { //Second tab
        // Localized text with the dynamic translation and locale updates support
        id: popularTab
        objectName: "popularTab"
        title: qsTr("Popular") + Retranslate.onLocaleOrLanguageChanged
        imageSource: "asset:///images/popular.png"
        delegate: Delegate {
            id: popularTabDelegate
            source: "popularMovies.qml" // tab1 contents is loaded from popularMovies.qml
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.Default
    } //End of second tab
    Tab { //Third tab
        id: upcomingTab
        objectName: "upcomingTab"
        title: qsTr("Upcomng") + Retranslate.onLocaleOrLanguageChanged
        imageSource: "asset:///images/upcoming1.png"
        delegate: Delegate {
            id: upcomingTabDelegate
            source: "upcomingMovies.qml" // tab1 contents is loaded from popularMovies.qml
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.Default
    } //End of third tab
    Tab { //Fourth tab
        id: cinemasTab
        objectName: "cinemasTab"
        title: qsTr("Cinemas") + Retranslate.onLocaleOrLanguageChanged
        imageSource: "asset:///images/cinemas.png"
        delegate: Delegate {
            id: cinemasTabDelegate
            source: "cinemaList.qml" // tab1 contents is loaded from popularMovies.qml
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.Default
    } //End of fourth tab
    Tab { //Fifth tab
        id: searchTab
        objectName: "searchTab"
        title: qsTr("Search") + Retranslate.onLocaleOrLanguageChanged
        imageSource: "asset:///images/search.png"
        delegate: Delegate {
            id: searchTabDelegate
            source: "searchPage.qml" // tab1 contents is loaded from popularMovies.qml
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.Default
    } //End of fifth tab
    
}
