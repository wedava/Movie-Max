/****************************************************************************
** Meta object code from reading C++ file 'populardatasource.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/data/populardatasource.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'populardatasource.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PopularDataSource[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       1,   64, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   19,   18,   18, 0x05,
      74,   65,   18,   18, 0x05,
      94,   18,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
     110,   18,   18,   18, 0x08,
     140,  127,   18,   18, 0x08,
     191,  185,   18,   18, 0x08,
     243,  238,   18,   18, 0x08,

 // methods: signature, parameters, type, tag, flags
     304,  294,   18,   18, 0x02,
     338,  333,   18,   18, 0x02,
     359,   18,  355,   18, 0x02,

 // properties: name, type, flags
     398,  379, 0x00495809,

 // properties: notify_signal_id
       0,

       0        // eod
};

static const char qt_meta_stringdata_PopularDataSource[] = {
    "PopularDataSource\0\0newError\0"
    "errorCodeChanged(WeatherError::Type)\0"
    "revision\0weatherChanged(int)\0"
    "noMoreWeather()\0onHttpFinished()\0"
    "reply,errors\0onSslErrors(QNetworkReply*,QList<QSslError>)\0"
    "reply\0onSqlConnectorReply(bb::data::DataAccessReply)\0"
    "type\0onDialogFinished(bb::system::SystemUiResult::Type)\0"
    "city,date\0requestData(QString,QString)\0"
    "city\0refresh(QString)\0int\0incrementRevision()\0"
    "NetworkError::Type\0errorCode\0"
};

void PopularDataSource::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PopularDataSource *_t = static_cast<PopularDataSource *>(_o);
        switch (_id) {
        case 0: _t->errorCodeChanged((*reinterpret_cast< WeatherError::Type(*)>(_a[1]))); break;
        case 1: _t->weatherChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->noMoreWeather(); break;
        case 3: _t->onHttpFinished(); break;
        case 4: _t->onSslErrors((*reinterpret_cast< QNetworkReply*(*)>(_a[1])),(*reinterpret_cast< const QList<QSslError>(*)>(_a[2]))); break;
        case 5: _t->onSqlConnectorReply((*reinterpret_cast< const bb::data::DataAccessReply(*)>(_a[1]))); break;
        case 6: _t->onDialogFinished((*reinterpret_cast< bb::system::SystemUiResult::Type(*)>(_a[1]))); break;
        case 7: _t->requestData((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 8: _t->refresh((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: { int _r = _t->incrementRevision();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

#ifdef Q_NO_DATA_RELOCATION
static const QMetaObjectAccessor qt_meta_extradata_PopularDataSource[] = {
        NetworkError::getStaticMetaObject,
#else
static const QMetaObject *qt_meta_extradata_PopularDataSource[] = {
        &NetworkError::staticMetaObject,
#endif //Q_NO_DATA_RELOCATION
    0
};

const QMetaObjectExtraData PopularDataSource::staticMetaObjectExtraData = {
    qt_meta_extradata_PopularDataSource,  qt_static_metacall 
};

const QMetaObject PopularDataSource::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PopularDataSource,
      qt_meta_data_PopularDataSource, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PopularDataSource::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PopularDataSource::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PopularDataSource::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PopularDataSource))
        return static_cast<void*>(const_cast< PopularDataSource*>(this));
    return QObject::qt_metacast(_clname);
}

int PopularDataSource::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< NetworkError::Type*>(_v) = errorCode(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void PopularDataSource::errorCodeChanged(WeatherError::Type _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PopularDataSource::weatherChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PopularDataSource::noMoreWeather()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
