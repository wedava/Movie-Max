/****************************************************************************
** Meta object code from reading C++ file 'moviemaxapp.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/moviemaxapp.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'moviemaxapp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MovieMaxApp[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x08,
      49,   39,   12,   12, 0x08,
      86,   12,   12,   12, 0x08,
      99,   12,   12,   12, 0x08,

 // methods: signature, parameters, type, tag, flags
     113,   12,   12,   12, 0x02,
     124,   12,   12,   12, 0x02,
     135,   12,   12,   12, 0x02,
     145,   12,   12,   12, 0x02,

       0        // eod
};

static const char qt_meta_stringdata_MovieMaxApp[] = {
    "MovieMaxApp\0\0onSystemLanguageChanged()\0"
    "activeTab\0sectionTriggered(bb::cascades::Tab*)\0"
    "onFinished()\0onFinished1()\0discover()\0"
    "upcoming()\0cinemas()\0popular()\0"
};

void MovieMaxApp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MovieMaxApp *_t = static_cast<MovieMaxApp *>(_o);
        switch (_id) {
        case 0: _t->onSystemLanguageChanged(); break;
        case 1: _t->sectionTriggered((*reinterpret_cast< bb::cascades::Tab*(*)>(_a[1]))); break;
        case 2: _t->onFinished(); break;
        case 3: _t->onFinished1(); break;
        case 4: _t->discover(); break;
        case 5: _t->upcoming(); break;
        case 6: _t->cinemas(); break;
        case 7: _t->popular(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MovieMaxApp::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MovieMaxApp::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_MovieMaxApp,
      qt_meta_data_MovieMaxApp, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MovieMaxApp::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MovieMaxApp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MovieMaxApp::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MovieMaxApp))
        return static_cast<void*>(const_cast< MovieMaxApp*>(this));
    return QObject::qt_metacast(_clname);
}

int MovieMaxApp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
